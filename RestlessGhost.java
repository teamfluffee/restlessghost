package scripts.modules.restlessghost;

import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;
import scripts.fluffeesapi.client.clientextensions.Game;
import scripts.fluffeesapi.data.structures.bag.BagIds;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.Mission;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.MissionManager;
import scripts.modules.restlessghost.missions.entertower.EnterTowerMission;
import scripts.modules.restlessghost.missions.exittower.ExitTowerMission;
import scripts.modules.restlessghost.missions.graveyardskull.GraveyardSkullMission;
import scripts.modules.restlessghost.missions.graveyardtalk.GraveyardMission;
import scripts.modules.restlessghost.missions.skull.SkullMission;
import scripts.modules.restlessghost.missions.talkaereck.TalkAereckMission;
import scripts.modules.restlessghost.missions.urhney.UrhneyMission;

import java.util.LinkedList;

public class RestlessGhost implements MissionManager {

    public final static int QUEST_SETTING = 107;
    public static final RSArea GRAVEYARD = new RSArea(new RSTile(3247, 3196, 0), new RSTile(3253, 3190, 0));
    private LinkedList<Mission> missionList = new LinkedList<>();

    @Override
    public LinkedList<Mission> getMissions() {
        if (missionList.isEmpty()) {
            missionList.add(new GraveyardMission());
            missionList.add(new UrhneyMission());
            missionList.add(new TalkAereckMission());
            missionList.add(new SkullMission());
            missionList.add(new GraveyardSkullMission());
            missionList.add(new ExitTowerMission());
            missionList.add(new EnterTowerMission());
        }
        return missionList;
    }

    @Override
    public boolean shouldLoopMissions() {
        return true;
    }

    @Override
    public String getMissionName() {
        return "Restless Ghost";
    }

    @Override
    public String getBagId() {
        return BagIds.RESTLESS_GHOST.getId();
    }

    @Override
    public boolean isMissionValid() {
        return Game.getSetting(QUEST_SETTING) < 5;
    }

    @Override
    public boolean isMissionCompleted() {
        return Game.getSetting(QUEST_SETTING) >= 5 && Game.getVarBitValue(6719) == 0;
    }
}
