package scripts.modules.restlessghost.missions.graveyardskull.decisions;

import org.tribot.api2007.Player;
import scripts.fluffeesapi.client.clientextensions.Filters;
import scripts.fluffeesapi.client.clientextensions.Objects;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.restlessghost.RestlessGhost;
import scripts.modules.restlessghost.missions.commonnodes.InteractCoffin;

public class ShouldInteractCoffin extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return RestlessGhost.GRAVEYARD.contains(Player.getPosition()) &&
                Objects.find(5, Filters.Objects.nameEquals("Coffin")
                .and(Filters.Objects.hasAction("Open"))).length > 0;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new InteractCoffin());
        setFalseNode(new ShouldCloseQuestCompleted());
    }

}
