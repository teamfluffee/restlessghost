package scripts.modules.restlessghost.missions.graveyardskull.decisions;

import org.tribot.api2007.Interfaces;
import scripts.fluffeesapi.game.genericTasks.CloseQuestCompleted;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;

public class ShouldCloseQuestCompleted extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Interfaces.isInterfaceValid(277);
    }

    @Override
    public void initializeNode() {
        setTrueNode(new CloseQuestCompleted());
        setFalseNode(new ShouldWatchCutscene());
    }

}
