package scripts.modules.restlessghost.missions.graveyardskull.decisions;

import scripts.fluffeesapi.client.clientextensions.Game;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.restlessghost.missions.commonnodes.WatchCutscene;
import scripts.modules.restlessghost.missions.graveyardskull.processes.PlaceSkull;

public class ShouldWatchCutscene extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Game.getVarBitValue(6719) > 0;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new WatchCutscene());
        setFalseNode(new PlaceSkull());
    }

}
