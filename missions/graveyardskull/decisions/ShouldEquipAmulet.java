package scripts.modules.restlessghost.missions.graveyardskull.decisions;

import scripts.fluffeesapi.client.clientextensions.Inventory;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.restlessghost.missions.commonnodes.EquipGhostspeak;

public class ShouldEquipAmulet extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Inventory.inventoryContains("Ghostspeak amulet");
    }

    @Override
    public void initializeNode() {
        setTrueNode(new EquipGhostspeak());
        setFalseNode(new ShouldInteractCoffin());
    }

}
