package scripts.modules.restlessghost.missions.graveyardskull;

import org.tribot.api2007.Game;
import org.tribot.api2007.Player;
import scripts.fluffeesapi.client.clientextensions.Interfaces;
import scripts.fluffeesapi.data.structures.bag.BagIds;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.TreeMission;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.BaseDecisionNode;
import scripts.modules.restlessghost.RestlessGhost;
import scripts.modules.restlessghost.missions.graveyardskull.decisions.ShouldWalkGraveyard;

public class GraveyardSkullMission implements TreeMission {

    @Override
    public BaseDecisionNode getTreeRoot() {
        return new ShouldWalkGraveyard();
    }

    @Override
    public String getMissionName() {
        return "Graveyard Skull Mission";
    }

    @Override
    public String getBagId() {
        return BagIds.RESTLESS_GHOST.getId();
    }

    @Override
    public boolean isMissionValid() {
        return Game.getSetting(RestlessGhost.QUEST_SETTING) == 4 && Player.getPosition().getY() < 9550
                && Player.getPosition().getY() > 3167;
    }

    @Override
    public boolean isMissionCompleted() {
        return Game.getSetting(RestlessGhost.QUEST_SETTING) == 5 && !Interfaces.isInterfaceValid(277);
    }
}
