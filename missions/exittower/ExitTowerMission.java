package scripts.modules.restlessghost.missions.exittower;

import org.tribot.api2007.Game;
import org.tribot.api2007.Player;
import scripts.fluffeesapi.data.structures.bag.BagIds;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.TreeMission;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.BaseDecisionNode;
import scripts.modules.restlessghost.RestlessGhost;
import scripts.modules.restlessghost.missions.exittower.decisions.ShouldWalkLadder;

public class ExitTowerMission implements TreeMission {

    @Override
    public BaseDecisionNode getTreeRoot() {
        return new ShouldWalkLadder();
    }

    @Override
    public String getMissionName() {
        return "Exit tower.";
    }

    @Override
    public String getBagId() {
        return BagIds.RESTLESS_GHOST.getId();
    }

    @Override
    public boolean isMissionValid() {
        return Game.getSetting(RestlessGhost.QUEST_SETTING) == 4 &&
                (Player.getPosition().getY() > 9550 || Player.getPosition().getY() < 3167);
    }

    @Override
    public boolean isMissionCompleted() {
        return Game.getSetting(RestlessGhost.QUEST_SETTING) == 4
                && Player.getPosition().getY() < 9550 && Player.getPosition().getY() > 3167;
    }
}
