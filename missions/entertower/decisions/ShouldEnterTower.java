package scripts.modules.restlessghost.missions.entertower.decisions;

import org.tribot.api2007.Player;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.restlessghost.missions.entertower.processes.EnterTower;
import scripts.modules.restlessghost.missions.entertower.processes.WalkToSedridor;

public class ShouldEnterTower extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Player.getPosition().getY() < 9550;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new EnterTower());
        setFalseNode(new WalkToSedridor());
    }

}
