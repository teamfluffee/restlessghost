package scripts.modules.restlessghost.missions.entertower;

import org.tribot.api2007.Game;
import org.tribot.api2007.Player;
import scripts.fluffeesapi.data.structures.bag.BagIds;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.TreeMission;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.BaseDecisionNode;
import scripts.modules.restlessghost.RestlessGhost;
import scripts.modules.restlessghost.missions.entertower.decisions.ShouldWalkToTower;

public class EnterTowerMission implements TreeMission {

    @Override
    public BaseDecisionNode getTreeRoot() {
        return new ShouldWalkToTower();
    }

    @Override
    public String getMissionName() {
        return "Entering Tower";
    }

    @Override
    public String getBagId() {
        return BagIds.RESTLESS_GHOST.getId();
    }

    @Override
    public boolean isMissionValid() {
        return Game.getSetting(RestlessGhost.QUEST_SETTING) == 3 && Player.getPosition().getY() < 9565;
    }

    @Override
    public boolean isMissionCompleted() {
        return Player.getPosition().getX() < 3107
                && Player.getPosition().getY() > 9565;
    }
}
