package scripts.modules.restlessghost.missions.commonnodes;

import org.tribot.api.General;
import org.tribot.api2007.types.RSVarBit;
import scripts.fluffeesapi.client.clientextensions.Game;
import scripts.fluffeesapi.client.clientextensions.NPCChat;
import scripts.fluffeesapi.scripting.antiban.AntiBanSingleton;
import scripts.fluffeesapi.scripting.frameworks.task.Priority;
import scripts.fluffeesapi.scripting.frameworks.task.tasktypes.Task;

public class WatchCutscene extends Task {
    @Override
    public Priority priority() {
        return Priority.HIGH;
    }

    @Override
    public boolean isValid() {
        return Game.getVarBitValue(6719 ) > 0 && !NPCChat.inChat(1);
    }

    @Override
    public void successExecute() {
        long startTime = System.currentTimeMillis();
        RSVarBit cutsceneVarbit = RSVarBit.get(6719);
        while (cutsceneVarbit.getValue() > 0) {
            General.sleep(200, 300);
            AntiBanSingleton.get().resolveTimedActions();
        }

        int actionTime = (int) (System.currentTimeMillis() - startTime);
        AntiBanSingleton.get().setLastReactionTime(AntiBanSingleton.get().generateReactionTime(actionTime, false));
    }

    @Override
    public String getStatus() {
        return "Watching cutscene";
    }
}
