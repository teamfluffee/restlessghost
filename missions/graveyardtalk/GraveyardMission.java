package scripts.modules.restlessghost.missions.graveyardtalk;

import org.tribot.api2007.Game;
import scripts.fluffeesapi.data.structures.bag.BagIds;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.TreeMission;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.BaseDecisionNode;
import scripts.modules.restlessghost.RestlessGhost;
import scripts.modules.restlessghost.missions.graveyardtalk.decisions.ShouldWalkGraveyard;

public class GraveyardMission implements TreeMission {

    @Override
    public BaseDecisionNode getTreeRoot() {
        return new ShouldWalkGraveyard();
    }

    @Override
    public String getMissionName() {
        return "Graveyard Chat Mission";
    }

    @Override
    public String getBagId() {
        return BagIds.RESTLESS_GHOST.getId();
    }

    @Override
    public boolean isMissionValid() {
        return Game.getSetting(RestlessGhost.QUEST_SETTING) == 2;
    }

    @Override
    public boolean isMissionCompleted() {
        return Game.getSetting(RestlessGhost.QUEST_SETTING) == 3;
    }
}
