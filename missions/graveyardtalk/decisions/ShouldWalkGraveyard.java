package scripts.modules.restlessghost.missions.graveyardtalk.decisions;

import org.tribot.api2007.Player;
import scripts.fluffeesapi.game.genericProcessNodes.WalkToLocation;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;

import static scripts.modules.restlessghost.RestlessGhost.GRAVEYARD;

public class ShouldWalkGraveyard extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return !GRAVEYARD.contains(Player.getPosition());
    }

    @Override
    public void initializeNode() {
        setTrueNode(new WalkToLocation(GRAVEYARD.getRandomTile(), "Walk Graveyard"));
        setFalseNode(new ShouldEquipAmulet());
    }

}
