package scripts.modules.restlessghost.missions.graveyardtalk.decisions;

import org.tribot.api2007.NPCs;
import scripts.fluffeesapi.game.genericProcessNodes.TalkToGuide;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.restlessghost.missions.commonnodes.InteractCoffin;

public class ShouldOpenCoffin extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return NPCs.findNearest("Restless Ghost").length < 1;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new InteractCoffin());
        setFalseNode(new TalkToGuide("Restless Ghost", "Talking to Ghost",
                new String[]{"Yep, now tell me what the problem is."}));
    }

}
