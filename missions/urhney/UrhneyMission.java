package scripts.modules.restlessghost.missions.urhney;

import org.tribot.api2007.Game;
import scripts.fluffeesapi.data.structures.bag.BagIds;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.TreeMission;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.BaseDecisionNode;
import scripts.modules.restlessghost.RestlessGhost;
import scripts.modules.restlessghost.missions.urhney.decisions.ShouldWalkUrhney;

public class UrhneyMission implements TreeMission {
    @Override
    public BaseDecisionNode getTreeRoot() {
        return new ShouldWalkUrhney();
    }

    @Override
    public String getMissionName() {
        return "Father Urhney";
    }

    @Override
    public String getBagId() {
        return BagIds.RESTLESS_GHOST.getId();
    }

    @Override
    public boolean isMissionValid() {
        return Game.getSetting(RestlessGhost.QUEST_SETTING) == 1;
    }

    @Override
    public boolean isMissionCompleted() {
        return Game.getSetting(RestlessGhost.QUEST_SETTING) == 2;
    }
}
