package scripts.modules.restlessghost.missions.skull;

import org.tribot.api2007.Game;
import org.tribot.api2007.Player;
import scripts.fluffeesapi.data.structures.bag.BagIds;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.TreeMission;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.BaseDecisionNode;
import scripts.modules.restlessghost.RestlessGhost;
import scripts.modules.restlessghost.missions.skull.decisions.ShouldWalkToSkull;

public class SkullMission implements TreeMission {

    @Override
    public BaseDecisionNode getTreeRoot() {
        return new ShouldWalkToSkull();
    }

    @Override
    public String getMissionName() {
        return "Get skull";
    }

    @Override
    public String getBagId() {
        return BagIds.RESTLESS_GHOST.getId();
    }

    @Override
    public boolean isMissionValid() {
        return Game.getSetting(RestlessGhost.QUEST_SETTING) == 3 && Player.getPosition().getX() > 3102
                && Player.getPosition().getY() > 9555;
    }

    @Override
    public boolean isMissionCompleted() {
        return Game.getSetting(RestlessGhost.QUEST_SETTING) == 4;
    }
}
