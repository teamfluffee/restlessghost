package scripts.modules.restlessghost.missions.skull.decisions;

import scripts.fluffeesapi.client.clientextensions.Objects;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.restlessghost.missions.skull.processes.WalkSkull;

public class ShouldWalkToSkull extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Objects.find(7, "Altar").length < 1;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new WalkSkull());
        setFalseNode(new ShouldGrabSkull());
    }

}
