package scripts.modules.restlessghost.missions.skull.decisions;

import org.tribot.api2007.types.RSTile;
import scripts.fluffeesapi.client.clientextensions.Inventory;
import scripts.fluffeesapi.game.genericProcessNodes.WalkToLocation;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.restlessghost.missions.skull.processes.GrabSkull;

public class ShouldGrabSkull extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return !Inventory.inventoryContains("Ghosts's skull");
    }

    @Override
    public void initializeNode() {
        setTrueNode(new GrabSkull());
        setFalseNode(new WalkToLocation(new RSTile(3112, 9559, 0), "Leave skull"));
    }

}
